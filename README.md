# 3D Tour
This is a simple codebase that show the results of the [Medium Article](https://medium.com/@z-uo/create-an-explorable-home-in-an-online-3d-world-with-open-source-6f729f7031f6) that explain how to create a simple explorable 3D home with [SweetHome3D](http://www.sweethome3d.com/download.jsp) [Blender](https://www.blender.org/download/) [Armory 3D](https://armory3d.org/) and deploy it on [Altervista](https://it.altervista.org/) all completelly free.

![Demo image](imgs/demo.png)

* [Demo link](https://medium.com/r/?url=http%3A%2F%2Fserverservices.altervista.org%2FArmory3DTutorialMedium%2Fhtml5%2Findex2.html)
